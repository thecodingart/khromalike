//
//  PaddedLabel.swift
//  KhromaLike
//
//  Created by Brandon Levasseur on 11/8/14.
//  Copyright (c) 2014 RayWenderlich. All rights reserved.
//

import UIKit

class PaddedLabel: UILabel {
    var verticalPadding = 0.0
    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        if traitCollection.verticalSizeClass == .Compact {
            verticalPadding = 0.0
        } else {
            verticalPadding = 20.0
        }
        
        //Need to update the layout
        invalidateIntrinsicContentSize()
    }
    
    override func intrinsicContentSize() -> CGSize {
        var intrinsicSize = super.intrinsicContentSize()
        //Add the padding
        intrinsicSize.height += CGFloat(verticalPadding)
        return intrinsicSize
    }
}
